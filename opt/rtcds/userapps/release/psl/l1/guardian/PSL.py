# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-

import time

from guardian import GuardState

import ifolib.rs_power_control as rsp
from ifolib.ifo_power_control import IFOPowerStep

##################################################

nominal = 'FINAL_POWER_EPICS'

# gain in db for IMC servo during acquisition, set during power reset (1 W input)
imc_servo_acquire_in1gain = -13#-26
#imc_servo_acquire_in1gain = -15  #refcav bypass AJM20190205

# Threshold to do something about current laser power, in fraction
THRESHOLD = 0.15

#Tolerance for rerunning
EPICS_TOLERANCE = 0.14

# reset power in Watts
RESET_POWER = 1

# safe power in watts
SAFE_POWER = 0

#Minimum input power
MINIMUM_INPUT_POWER = 2

# list of requestable powers in Watts
REQUEST_POWERS = [10,20,25]

##################################################

def goto_power(system,power):
    try:
        rsp.rs_goto_power(system, power)
    except:
        return False
    return True


def check_power(desired_power):
    current_power = ezca['IMC-PWR_IN_OUTPUT']
    if (float(current_power) < desired_power*(1-THRESHOLD)) or (float(current_power) > desired_power*(1+THRESHOLD)):
        return False
    else:
        return True

def check_safe_power():
    current_power = ezca['IMC-PWR_IN_OUTPUT']
    if (float(current_power) > 0.05):
        return False
    else:
        return True

def check_input_power():
    input_power = ezca['IMC-PWR_EOM_OUTPUT']
    if (float(input_power) < MINIMUM_INPUT_POWER):
        return False
    else:
        return True


def set_alarm(power):
    ezca['IMC-PWR_IN_OUTPUT.HSV'] = 'MAJOR'
    ezca['IMC-PWR_IN_OUTPUT.HSV'] = 'MAJOR'
    ezca['IMC-PWR_IN_OUTPUT.HIGH'] = power*(1+THRESHOLD)
    ezca['IMC-PWR_IN_OUTPUT.LOW'] = power*(1-THRESHOLD) 
    return True

def unset_alarm():
    ezca['IMC-PWR_IN_OUTPUT.HSV'] = 'NO_ALARM'
    ezca['IMC-PWR_IN_OUTPUT.LSV'] = 'NO_ALARM'
    return True

def check_iss_loop_and_turn_off():
    if ezca['PSL-ISS_LOOP_STATE_OUTPUT'] < 1000:
        if ezca['PSL-ISS_SECONDLOOP_CLOSED'] > 1000:
            ezca['PSL-ISS_SECONDLOOP_CLOSED'] = 0
            notify('Turned off ISS 2nd loop when 1st loop turned off')

def gen_goto_state(power,index,step_gains=True,step_refl_gain=False):
    class POWER(GuardState):
        goto = True
        request = False

        def main(self):
            p = power
            unset_alarm()

            if ezca['PSL-POWER_REQUEST'] == p:
                self.done = True
                return True
            log("Moving IFO input power to %s watts..." % p)
            self.step = IFOPowerStep(p,step_gains=step_gains,step_refl_gain=step_refl_gain)
            self.final = False
            self.done = False

        def run(self):
            try:
                if self.done:
                    return True
                elif self.final:
                    if self.timer['settle']:
                        log("adjusting PSL-POWER_SCALE to final output power")
                        ezca['PSL-POWER_SCALE_OFFSET'] = rsp.rs_get_power('PSL')
                        self.done = True
                        return True
                elif self.step.step():
                    # adjust the SCALE to the actual output power
                    # FIXME: do we want to do this here, i.e. set the
                    # SCALE to the actual output power, or do we want to
                    # just leave it as the request?
                    # wait for the power to settle
                    settle = 10
                    log("waiting for power to settle %s sec..." % settle)
                    self.timer['settle'] = settle
                    self.final = True
            except:
                return 'FAULT'
    POWER.index = index
    return POWER

def gen_idle_state(power, index):
    class IDLE_STATE(GuardState):
        request = True
        def main(self):
            self.p = power
            set_alarm(power)
            self.failed = False
            self.busy = False
            self.last_power_request = power
            return True

        def run(self):
            check_iss_loop_and_turn_off()
            if not check_power(self.p):
                if not self.failed:
                    notify("Power into IMC outside of requested value +/- threshold, requesting power again...")
                    ezca['PSL-POWER_REQUEST'] = self.p
                    if not goto_power('PSL', self.p):
                        return 'FAULT'
                    time.sleep(1)
                    self.timer['out_of_range'] = 60
                    if not check_power(self.p):
                        notify("Power into IMC outside of requested value, search for home suggested")
                        self.failed = True
                        if self.timer['out_of_range']:
                            self.failed = False
                        return False
                else:
                    if check_power(self.p):
                        notify("Power now within threshold")
                        self.failed = False
                        return True
                    return False
            else:
                return True
    IDLE_STATE.index = index
    return IDLE_STATE

edges = []

##################################################
class INIT(GuardState):
    request = True

    def main(self):
        # FIXME: look at the current power and go to the appropriate
        # state
        pass

##################################################
class FAULT(GuardState):
    index = 1
    request = False
    redirect = False

    def run(self):
        if ['PSL-ROTATIONSTAGE_RS_EXECUTE'] == 1:
            notify('Rotation Stage still executing, hit abort on RS screen')
            return False
        else:
            return True

##################################################
class POWER_SAFE(GuardState):
    index = 2
    goto = True

    def main(self):
        unset_alarm()
        self.failed = False
        log("Moving PSL rotation stage to %s watts..." % SAFE_POWER)
        rsp.rs_update_calc('PSL')
        ezca['PSL-POWER_REQUEST'] = SAFE_POWER
        ezca['PSL-POWER_SCALE_OFFSET'] = SAFE_POWER
        ezca['PSL-ISS_SECONDLOOP_GAIN'] = 30
        ezca['PSL-ISS_SECONDLOOP_CLOSED'] = 0
        ezca['IMC-REFL_SERVO_IN1GAIN'] = imc_servo_acquire_in1gain
        
        if not goto_power('PSL',SAFE_POWER):
            return 'FAULT'
        ezca['IMC-PWR_IN_OUTPUT.HSV'] = 'MAJOR'
        ezca['IMC-PWR_IN_OUTPUT.HSV'] = 'MAJOR'
        ezca['IMC-PWR_IN_OUTPUT.HIGH'] = 0.05
        ezca['IMC-PWR_IN_OUTPUT.LOW'] = -0.05

    def run(self):
        check_iss_loop_and_turn_off()
        if not check_safe_power():
            if not self.failed:
                notify("Power into IMC outside of requested value +/- threshold, requesting power again...")
                ezca['PSL-POWER_REQUEST'] = SAFE_POWER
                if not goto_power('PSL', SAFE_POWER):
                    return 'FAULT'
                time.sleep(1)
                self.timer['out_of_range'] = 60
                if not check_safe_power():
                    notify("Power into IMC outside of requested value, search for home suggested")
                    self.failed = True
                    if self.timer['out_of_range']:
                        self.failed = False
                    return False
            else:
                if check_safe_power():
                    notify("Power now within threshold")
                    self.failed = False
                    if not check_input_power():
                        return False
                    return True
                return False
        else:
            if not check_input_power():
                return False
            return True
         
        if not check_input_power():
            return False
        return True


class POWER_RESET(GuardState):
    index = 3
    goto = True

    def main(self):
        if not check_input_power():
            return 'POWER_SAFE'
        unset_alarm()
        self.failed = False
        self.counter = 0
        log("Moving PSL rotation stage to %s watt..." % 1)
        rsp.rs_update_calc('PSL')
        ezca['PSL-POWER_REQUEST'] = RESET_POWER
        ezca['PSL-POWER_SCALE_OFFSET'] = RESET_POWER
        ezca['PSL-ISS_SECONDLOOP_GAIN'] = 30
        ezca['PSL-ISS_SECONDLOOP_CLOSED'] = 0
        ezca['IMC-REFL_SERVO_IN1GAIN'] = imc_servo_acquire_in1gain
        if not goto_power('PSL', RESET_POWER):
            return 'FAULT'
        set_alarm(RESET_POWER)
        self.done = False

    def run(self):
        if not check_input_power():
            return 'POWER_SAFE'
        check_iss_loop_and_turn_off()
        if not check_power(RESET_POWER):
            if not self.failed:
                notify("Power into IMC outside of requested value +/- threshold, requesting power again...")
                ezca['PSL-POWER_REQUEST'] = RESET_POWER
                if not goto_power('PSL', RESET_POWER):
                    return 'FAULT'
                time.sleep(1)
                self.timer['out_of_range'] = 60
                if not check_power(RESET_POWER):
                    notify("Power into IMC outside of requested value, search for home suggested")
                    self.failed = True
                    if self.timer['out_of_range']:
                        self.failed = False
                    return False
            else:
                if check_power(RESET_POWER):
                    notify("Power now within threshold")
                    self.failed = False
                    return True
                return False
        else:
            return True
        return True


class GOTO_POWER_EPICS(GuardState):
     index = 999
     goto = True
     request = True

     def main(self):
        if not check_input_power():
            return 'POWER_SAFE'
        p = ezca['PSL-GUARD_POWER_REQUEST']
        if ezca['PSL-GUARD_STEP_REFL_GAIN'] == 0:
            step_refl_gain = False
        else:
            step_refl_gain = True
        if ezca['PSL-GUARD_STEP_DC_GAIN'] == 0:
            step_dc_gain = False
        else:
            step_dc_gain = True
        unset_alarm()
        step_gains = True

        if ezca['PSL-POWER_REQUEST'] == p:
            self.done = True
            return True
        log("Moving IFO input power to %s watts..." % p)
        self.step = IFOPowerStep(p, step_gains=step_gains, step_refl_gain=step_refl_gain, step_dc_gain=step_dc_gain)
        self.final = False
        self.done = False

     def run(self):
        if not check_input_power():
            return 'POWER_SAFE'
        try:
            if self.done:
                return True
            elif self.final:
                if self.timer['settle']:
                    log("adjusting PSL-POWER_SCALE to final output power")
                    ezca['PSL-POWER_SCALE_OFFSET'] = rsp.rs_get_power('PSL')
                    self.done = True
                    return True
            elif self.step.step():
                    # adjust the SCALE to the actual output power
                    # FIXME: do we want to do this here, i.e. set the
                    # SCALE to the actual output power, or do we want to
                    # just leave it as the request?
                    # wait for the power to settle
                settle = 10
                log("waiting for power to settle %s sec..." % settle)
                self.timer['settle'] = settle
                self.final = True
        except:
            return 'FAULT'


class POWER_EPICS(GuardState):
    index = 1000
    goto = False
    request = True

    def main(self):
        if not check_input_power():
            return 'POWER_SAFE'
        guard_power_request = ezca['PSL-GUARD_POWER_REQUEST']
        self.p = ezca['PSL-POWER_REQUEST']
        if self.p < guard_power_request*(1-EPICS_TOLERANCE):
            return 'GOTO_POWER_EPICS'
        elif self.p > guard_power_request*(1+EPICS_TOLERANCE):
            return 'GOTO_POWER_EPICS'
        set_alarm(self.p)
        self.failed = False
        self.busy = False
        self.last_power_request = self.p
        return True

    def run(self):
        if not check_input_power():
            return 'POWER_SAFE'
        check_iss_loop_and_turn_off()
        if not check_power(self.p):
            if not self.failed:
                notify("Power into IMC outside of requested value +/- threshold, requesting power again...")
                ezca['PSL-POWER_REQUEST'] = self.p
                if not goto_power('PSL', self.p):
                    return 'FAULT'
                time.sleep(1)
                self.timer['out_of_range'] = 60
                if not check_power(self.p):
                    notify("Power into IMC outside of requested value, search for home suggested")
                    self.failed = True
                    if self.timer['out_of_range']:
                        self.failed = False
                    return False
            else:
                if check_power(self.p):
                    notify("Power now within threshold")
                    self.failed = False
                    return True
                return False
        else:
            return True


class GOTO_FINAL_POWER_EPICS(GuardState):
     index = 1009
     goto = True
     request = True

     def main(self):
        if not check_input_power():
            return 'POWER_SAFE'
        self.p = ezca['PSL-GUARD_POWER_REQUEST']
        if ezca['PSL-GUARD_STEP_REFL_GAIN'] == 0:
            step_refl_gain = False
        else:
            step_refl_gain = True
        if ezca['PSL-GUARD_STEP_DC_GAIN'] == 0:
            step_dc_gain = False
        else:
            step_dc_gain = True
        unset_alarm()
        step_gains = True

        if ezca['PSL-POWER_REQUEST'] == self.p:
            self.done = True
            return True
        log("Moving IFO input power to %s watts..." % self.p)
        self.step = IFOPowerStep(self.p,step_gains=step_gains,step_refl_gain=step_refl_gain,step_dc_gain=step_dc_gain)
        self.final = False
        self.done = False
        self.final_correction_done = False
        self.timer["minimum_wait"] = 0

     def run(self):
        if not check_input_power():
            return 'POWER_SAFE'
        try:
            if self.done:
                return True
            elif (self.final and (not self.final_correction_done)):
                if self.timer['settle']:
                    log("tweaking to final value")
                    #Grab what the current power is, compare to max, rotate to new power, set ARM matrix DC gain correct
                    current_power = ezca['IMC-IM4_TRANS_SUM_OUTPUT'] #JCB changed to IM4 from IMC power in
                    ratio = self.p/current_power
                    log('ratio is ' + str(ratio))
                    arm_correction = self.p/ezca['PSL-ROTATIONSTAGE_POWER_REQUEST']
                    final_request = ezca['PSL-ROTATIONSTAGE_POWER_REQUEST']*ratio
                    ezca['PSL-ROTATIONSTAGE_POWER_REQUEST'] = ezca['PSL-ROTATIONSTAGE_POWER_REQUEST']*ratio
                    ezca['LSC-ARM_INPUT_MTRX_SETTING_1_1'] = ezca['LSC-ARM_INPUT_MTRX_SETTING_1_1']/arm_correction
                    time.sleep(0.1)
                    ezca['PSL-ROTATIONSTAGE_COMMAND'] = 2
                    ezca['LSC-ARM_INPUT_MTRX_LOAD_MATRIX'] = 1
                    settle = 10
                    self.timer['settle_correction'] = settle
                    self.final_correction_done = True
            elif self.final_correction_done:
                if self.timer['settle_correction']:
                    log("adjusting PSL-POWER_SCALE to final output power")
                    ezca['PSL-POWER_SCALE_OFFSET'] = ezca['IMC-IM4_TRANS_SUM_OUTPUT']
                    self.done = True
                    return True
            elif self.timer["minimum_wait"]:
                if self.step.step():
                    # adjust the SCALE to the actual output power
                    # FIXME: do we want to do this here, i.e. set the
                    # SCALE to the actual output power, or do we want to
                    # just leave it as the request?
                    # wait for the power to settle
                    # JCB 20240510 Changed settle from 5 to 30, also added new minimum wait of 30 seconds between steps
                    settle = 30
                    log("waiting for power to settle %s sec..." % settle)
                    self.timer['settle'] = settle
                    self.final = True
                self.timer["minimum_wait"] = 30
                
        except:
            return 'FAULT'


class FINAL_POWER_EPICS(GuardState):
    index = 1010
    goto = False
    request = True

    def main(self):
        if not check_input_power():
            return 'POWER_SAFE'
        guard_power_request = ezca['PSL-GUARD_POWER_REQUEST']
        self.p = ezca['PSL-POWER_REQUEST']
        if self.p < guard_power_request*(1-EPICS_TOLERANCE):
            return 'GOTO_FINAL_POWER_EPICS'
        elif self.p > guard_power_request*(1+EPICS_TOLERANCE):
            return 'GOTO_FINAL_POWER_EPICS'
        set_alarm(self.p)
        self.failed = False
        self.busy = False
        self.last_power_request = self.p
        return True

    def run(self):
        if not check_input_power():
            return 'POWER_SAFE'
        check_iss_loop_and_turn_off()
        if not check_power(self.p):
            if not self.failed:
                notify("Power into IMC outside of requested value +/- threshold, requesting power again...")
                ezca['PSL-POWER_REQUEST'] = self.p
                if not goto_power('PSL', self.p):
                    return 'FAULT'
                time.sleep(1)
                self.timer['out_of_range'] = 60
                if not check_power(self.p):
                    notify("Power into IMC *still* outside of requested value +/- threshold...")
                    self.failed = True
                    if self.timer['out_of_range']:
                        self.failed = False
                    return False
            else:
                if check_power(self.p):
                    notify("Power now within threshold")
                    self.failed = False
                    return True
                return False
        else:
            return True


edges += [('GOTO_POWER_EPICS', 'POWER_EPICS')]
edges += [('GOTO_FINAL_POWER_EPICS', 'FINAL_POWER_EPICS')]

##################################################
GOTO_POWER_1W_NO_REFL_SCALE = gen_goto_state(1,9,step_refl_gain=False)
POWER_1W_NO_REFL_SCALE = gen_idle_state(1,10)
edges += [('GOTO_POWER_1W_NO_REFL_SCALE','POWER_1W_NO_REFL_SCALE')]

##################################################
GOTO_POWER_4W_NO_REFL_SCALE = gen_goto_state(4,39,step_refl_gain=False)
POWER_4W_NO_REFL_SCALE = gen_idle_state(4,40)
edges += [('GOTO_POWER_4W_NO_REFL_SCALE','POWER_4W_NO_REFL_SCALE')]

##################################################
# automatically generate basic power request states
for power in REQUEST_POWERS:
    pint = int(power)
    index = pint * 10
    state_goto = 'GOTO_POWER_%dW' % (pint)
    state_idle = 'POWER_%dW' % (pint)
    globals()[state_goto] = gen_goto_state(pint, index-1)
    globals()[state_idle] = gen_idle_state(pint, index)
    edges += [(state_goto, state_idle)]

edges += [('FINAL_POWER_EPICS','FINAL_POWER_EPICS')]
edges += [('POWER_EPICS','POWER_EPICS')]
edges += [('POWER_RESET','POWER_RESET')]
edges += [('POWER_SAFE','POWER_SAFE')]

##################################################
# SVN $Id$
# $HeadURL$
##################################################
