#!/usr/bin/env python

import time
import math

import cdsutils

from . import rs_power_control as rsp

##################################################

POWER_REQUEST_CHANNEL = 'PSL-POWER_REQUEST'
POWER_INPUT_CHANNEL = 'IMC-PWR_IN_OUTPUT'
DEFAULT_STEP_SIZE_DB = 1
DEFAULT_TIME_STEP = 2.0

####################
# def dbf(db):
#     """Return multiplicitive factor for specified dB."""
#     return 10**(abs(db)/20)

# def power_step_single(db):
#     """Single step of power by specified db

#     dB may be negative to lower the power.

#     """

#     # step input power
#     ezca['PSL-POWER_REQUEST'] *= dbf(db)

#     rsp.rs_goto_power('PSL', ezca['PSL-POWER_REQUEST'])

#     # adjust ISS analog second loop gain
#     ezca['PSL-ISS_SECONDLOOP_GAIN'] += db

#     # adjust MC servo common gain
#     ezca['IMC-REFL_SERVO_IN1GAIN'] += db

#     ##########
#     # adjust CM common servo gain
#     ezca['LSC-REFL_SERVO_IN1GAIN'] += db

#     # adjust OMC gain
#     ezca['LSC-OMC_DC_GAIN'] *= dbf(db)
#     ##########

#     # set power scale offset to input power
#     ezca['PSL-POWER_SCALE_TRAMP'] = 2
#     ezca['PSL-POWER_SCALE_OFFSET'] = ezca['PSL-POWER_REQUEST']

#     sleep(2)
####################

# class IFOPowerSetServo(object):
#     def __init__(self, power):
#         self._setpoint = power
#         self._residual = 0.05

#         self._gain = 1
#         self._ugf = 1

#         self._last_time = None
#         self._done = False

#     def _get_err(self):
#         return rsp.rs_get_power('PSL')

#     def _get_ctrl(self):
#         return ezca[POWER_REQUEST_CHANNEL]

#     def _set_ctrl(self, ctrl):
#         rsp.rs_goto_power('PSL', ctrl)

#     def step(self):
#         if self._done:
#             return True

#         current_time = time.time()

#         # if this is the first step, set up initial values
#         if self._last_time is None:
#             self._t0 = current_time
#             self._last_time = current_time

#         ctrl = self._get_ctrl()

#         err = self._get_err()
#         print "err:", err

#         err -= self._setpoint

#         dt = current_time - self._last_time

#         ctrl -= self._gain * self._ugf * err * dt

#         print "update ctrl:"
#         self._set_ctrl(ctrl)

#         self._last_time = current_time

#         if abs(self._setpoint - self._get_err()) < self._residual:
#             print self._setpoint, self._get_err(), abs(self._setpoint - self._get_err()), self._residual
#             print "DONE"
#             self._done = True
#             return True

#         return False

# def ifo_power_servo(power):
#     s = IFOPowerSetServo(power)
#     while not s.step():
#         time.sleep(1)


class IFOPowerStep(object):
    """Guardian-compatible IFO power step class.

    If `nsteps` is None (default) the first argument is the desired
    power level.  Power will step to that value in 1dB increments with
    a time step in seconds specified by `time_step`.

    If `nsteps` is an int it is interpreted as a number of steps, and
    the first argument is interpreted as a step size in dB.

    """
    def __init__(self, power, nsteps=None, time_step=DEFAULT_TIME_STEP, step_gains=True, step_refl_gain=True,step_dc_gain=False):
        self.request = power
        self.step_dc_gain = step_dc_gain
        self.step_refl_gain = step_refl_gain
        self.time_step = time_step
        self.step_gains = []
        self.done = False
        self.finalize = False

        if power <= 0:
            power = 0.01

        # FIXME: this *should* be the current power, but we're using
        # the current request
        #current_power = rsp.rs_get_power('PSL')
        current_power = ezca[POWER_REQUEST_CHANNEL]

        if current_power <= 0:
            current_power = 0.1

        # calculate steps
        if nsteps:
            dbstep = power
            db = abs(dbstep)*nsteps
        else:
            db = 20*math.log10(float(power) / current_power)
            nsteps = int(abs(round(db))) / DEFAULT_STEP_SIZE_DB
            if nsteps == 0:
                self.done = True
                return
            #dbstep = db/nsteps
            dbstep = math.copysign(DEFAULT_STEP_SIZE_DB, db)

        spec = '%.3f,%d' % (dbstep, nsteps)
        nspec = '%.3f,%d' % (-dbstep, nsteps)
        specdb = '%.3fdb,%d' % (dbstep, nsteps)
        nspecdb = '%.3fdb,%d' % (-dbstep, nsteps)
        nspecdbHalf = '%.3fdb,%d' % (-float(dbstep)/2.0, nsteps)

        print('request:', power)
        print('current:', current_power)
        print('calc:', current_power * 10**(dbstep * nsteps / 20.))
        print('db:', db)
        print('stepspec:', spec, specdb, time_step)
        #sys.exit()

        # update the power calculation
        rsp.rs_update_calc('PSL')

        # main rotation stage power request step
        # start by setting the power request channel to be the current
        # value.  This should cover the case where there is an
        # existing request that was not actuated.
        ezca[POWER_REQUEST_CHANNEL] = self.last_request = current_power
        time.sleep(0.5)
        self.step_request = cdsutils.Step(ezca, POWER_REQUEST_CHANNEL, specdb, time_step=time_step)
        time.sleep(0.5)

        # PSL/IMC/IOO gain steps
	#WARNING VERY FRAGILE, DO NOT CHANGE ORDER OF STEPS BEING ADDED TO ARRAY!!! JCB 2015/9/14
        if step_gains:
            self.step_gains = [
                cdsutils.Step(ezca, 'PSL-ISS_SECONDLOOP_GAIN', nspec, time_step=time_step),
#                cdsutils.Step(ezca, 'IMC-REFL_SERVO_IN1GAIN', nspec, time_step=time_step),
                ]

            if step_refl_gain:
#                self.step_gains += [cdsutils.Step(ezca, 'LSC-REFL_SERVO_IN1GAIN', nspec, time_step=time_step),]
###
                self.step_gains += [
                    cdsutils.Step(ezca, 'IMC-REFL_SERVO_FASTGAIN', nspec, time_step=time_step),
                    cdsutils.Step(ezca, 'LSC-MCL_GAIN', nspecdb, time_step=time_step),
                    ]
            else:
                self.step_gains = [
                    cdsutils.Step(ezca, 'IMC-REFL_SERVO_IN1GAIN', nspec, time_step=time_step),
                ]
###
            if step_dc_gain:
                self.step_gains += [cdsutils.Step(ezca, 'LSC-ARM_INPUT_MTRX_SETTING_1_1', nspecdbHalf, time_step=time_step),]
                ezca['LSC-ARM_INPUT_MTRX_TRAMP'] = 0.3

        ezca['PSL-POWER_SCALE_TRAMP'] = self.time_step

    def step(self):
        # if we're already done just return immediately
        if self.done:
            return True

        # finalize with one last tweak to get the output power to
        # match the request
        if self.finalize:
            #rsp.rs_bootstrap_power('PSL', self.request)
            self.done = True
            return True

        # step the power request
        ret = self.step_request.step()

        # adjust the power scale offset to the input power
        ezca['PSL-POWER_SCALE_OFFSET'] = ezca[POWER_REQUEST_CHANNEL]

        # actuate PSL rotation stage to new power setpoint if it's changed
        if ezca[POWER_REQUEST_CHANNEL] != self.last_request:
            rsp.rs_goto_power('PSL', ezca[POWER_REQUEST_CHANNEL])
            self.last_request = ezca[POWER_REQUEST_CHANNEL]

        # step gains
        #WARNING VERY FRAGILE, coordinate with init function
        temp_counter = 0
        for step in self.step_gains:
            ret &= step.step()
            if self.step_dc_gain:
                temp_counter = temp_counter + 1
                if (self.step_refl_gain and (temp_counter == 4)) or ((not self.step_refl_gain) and (temp_counter == 3)):
                    time.sleep(0.5)
                    ezca['LSC-ARM_INPUT_MTRX_LOAD_MATRIX'] = 1

        # if all steps return True, we're done
        if ret:
            self.finalize = True

        return self.done


def ifo_power_step(*args, **kwargs):
    step = IFOPowerStep(*args, **kwargs)
    while not step.step():
        time.sleep(step.time_step/2)
        print
        print

##################################################

if __name__ == '__main__':
    import sys
    import argparse

    usage = """ifo_power_control
       ifo_power_control [-s ...] power
       ifo_power_control [-s ...] db nsteps
"""
    description = """Set/step IFO input power.

If a single argument is provided it is interpreted as a desired
absolute power in Watts.  The power will be stepped to that value from
the current value in steps of 1dB, with a stride of <time_step>.

If two arguments are provided, the first is interpreted as a step size
in dB, and the second is the total number of steps.
"""

    parser = argparse.ArgumentParser(description=description,
                                     usage=usage,
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     )
    parser.add_argument('power', metavar='power/db', type=float, nargs='?',
                        help="absolute power in Watts, or step size in dB")
    parser.add_argument('nsteps', nargs='?', type=int,
                        help="number of steps")
    parser.add_argument('-s', '--time_step', type=float, default=DEFAULT_TIME_STEP,
                        help=" power in Watts, or step size in dB")
    args = parser.parse_args()

    from ezca import Ezca

    Ezca().export()

    if not args.power:
        print('IFO input power: %.3f Watts' % rsp.rs_get_power('PSL'))
        sys.exit()

    try:
        ifo_power_step(args.power,
                       nsteps=args.nsteps,
                       time_step=args.time_step)
        #ifo_power_servo(args.power)
    except KeyboardInterrupt:
        sys.exit()
